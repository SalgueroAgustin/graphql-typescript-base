import { DataSource } from 'apollo-datasource';
import { MutationAddBookArgs, MutationUpdateBookArgs, QueryGetBookArgs } from 'src/generated/graphql';
import { addObject, getAllObjects, getObject, updateObject } from '../utils/dbMiddleware';

export class BooksProvider extends DataSource {
  public async getBook(args: QueryGetBookArgs) {
    return getObject("books", args._id);
  }

  public async addBook(args: MutationAddBookArgs) {
    return addObject("books", args);
  }

  public async getBooks() {
    return getAllObjects("books")
  }

  public async updateBook(args: MutationUpdateBookArgs) {
    return updateObject("books", args)
  }
}