import { gql } from 'apollo-server';

export const bookType = gql`
    type Book {
        _id: String
        title: String
        author: String
    }

    type Query {
        getBook(_id: String!): Book
        getBooks: [Book]
    }

    type Mutation {
        addBook(title: String!, author: String): Book
        updateBook(_id: String!, title: String, author: String): Book
    }
`;
