import { Resolvers } from 'src/generated/graphql';
import { bookResolver } from './book';

export const resolvers: Resolvers = {
    Query: {
        ...bookResolver.Query,
    },
    Mutation: {
        ...bookResolver.Mutation,
    }
}