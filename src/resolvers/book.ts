import { Resolvers } from 'src/generated/graphql';

export const bookResolver: Resolvers = {
  Query: {
    getBook: (_, args, ctx) => ctx.dataSources.booksProvider.getBook(args),
    getBooks: (_, __, ctx) => ctx.dataSources.booksProvider.getBooks()
  },
  Mutation: {
    addBook: (_, args, ctx) => ctx.dataSources.booksProvider.addBook(args),
    updateBook: (_, args, ctx) => ctx.dataSources.booksProvider.updateBook(args)
  }
};