import dotenv from 'dotenv';
import { Db, MongoClient, ObjectId } from 'mongodb';

dotenv.config()

type DbCallback = (dbClient: Db) => any

export const withMongoDb = async (callback: DbCallback) => {
    try {
        const mongo = new MongoClient(
            process.env.DB_CONNECTION_STRING || "",
            { useUnifiedTopology: true }
        );
        const dbName = process.env.DB_NAME || "graphqlTsDatabase";
        await mongo.connect();
        const db = mongo.db(dbName)
        const result = await callback(db);
        await mongo.close();
        return result;
    } catch (e) {
        console.error(e);
        throw e;
    }
}


export const getObject = async (collectionName: string, objectId: string) => withMongoDb(async (db) => {
    const collection = db.collection(collectionName);
    const result = await collection.findOne({ _id: new ObjectId(objectId) });
    return result;
})

export const addObject = async (collectionName: string, args: any) => withMongoDb(async (db) => {
    const collection = db.collection(collectionName);
    const result = await collection.insertOne({ ...args });
    return {
        _id: result.insertedId.toString(),
        ...args,
    };
})

export const getAllObjects = async (collectionName: string) => withMongoDb(async (db) => {
    const collection = db.collection(collectionName);
    const result = await collection.find();
    return result.toArray();
})

export const updateObject = async (collectionName: string, args: any) => withMongoDb(async (db) => {
    const collection = db.collection(collectionName);
    const { _id, ...updateParams } = args;
    const result = await collection.findOneAndUpdate(
        { _id: new ObjectId(_id) },
        { $set: { ...updateParams } },
        { upsert: false }
    )
    return result.value;
})