/* global db */
db.createUser(
  {
    user: "dev",
    pwd: "Test123!",
    roles: [
      {
        role: "readWrite",
        db: process.env.DB_NAME || "graphqlTsDatabase",
      },
    ],
  },
);